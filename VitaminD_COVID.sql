----------------------------------------------------------------------------
-- Vitamin D Lead investigator: David Meltzer
-- by UChicago CRI 
-- developed on 2020-12-16
-- CAPriCORN/PCORnet tables usage: DEMOGRAPHIC, DIAGNOSIS, ENCOUNTER, LAB_RESULT_CM,
--                                 PRESCRIBING, MED_ADMIN, CAP_HASH_TOKEN
-- 
-- Before running the code please MODIFY:
-- Line 12:  Enter database name where PCORI tables are found
-- Line 162: Enter database name where CAPriCORN CAP_HASH_TOKEN table is found

USE [PCORNet_CDM_6.0_LDS]	--!! Enter database name where PCORI tables are found 

-----------------------------------------------------------------------------------------
--Create a temp table to hold the inclusion/exclusion criteria
-----------------------------------------------------------------------------------------
IF OBJECT_ID('tempdb.dbo.#COVID_Inclusion', 'U') IS NOT NULL DROP TABLE #COVID_Inclusion; 

CREATE TABLE #COVID_Inclusion(
	[CODE] [varchar](50) NULL,
	[CODE_CATEGORY] [varchar](50) NULL,
	[CODE_TYPE] [varchar](50) NULL
) 

-----------------------------------------------------------------------------------------
--Insert the LOINC codes for COVID labs that define the population
-----------------------------------------------------------------------------------------
INSERT #COVID_Inclusion([CODE],[CODE_CATEGORY],[CODE_TYPE]) VALUES('94306-8','LAB-COVID','LC');
INSERT #COVID_Inclusion([CODE],[CODE_CATEGORY],[CODE_TYPE]) VALUES('94307-6','LAB-COVID','LC');
INSERT #COVID_Inclusion([CODE],[CODE_CATEGORY],[CODE_TYPE]) VALUES('94308-4','LAB-COVID','LC');
INSERT #COVID_Inclusion([CODE],[CODE_CATEGORY],[CODE_TYPE]) VALUES('94309-2','LAB-COVID','LC');
INSERT #COVID_Inclusion([CODE],[CODE_CATEGORY],[CODE_TYPE]) VALUES('94310-0','LAB-COVID','LC');
INSERT #COVID_Inclusion([CODE],[CODE_CATEGORY],[CODE_TYPE]) VALUES('94311-8','LAB-COVID','LC');
INSERT #COVID_Inclusion([CODE],[CODE_CATEGORY],[CODE_TYPE]) VALUES('94312-6','LAB-COVID','LC');
INSERT #COVID_Inclusion([CODE],[CODE_CATEGORY],[CODE_TYPE]) VALUES('94313-4','LAB-COVID','LC');
INSERT #COVID_Inclusion([CODE],[CODE_CATEGORY],[CODE_TYPE]) VALUES('94314-2','LAB-COVID','LC');
INSERT #COVID_Inclusion([CODE],[CODE_CATEGORY],[CODE_TYPE]) VALUES('94315-9','LAB-COVID','LC');
INSERT #COVID_Inclusion([CODE],[CODE_CATEGORY],[CODE_TYPE]) VALUES('94316-7','LAB-COVID','LC');
INSERT #COVID_Inclusion([CODE],[CODE_CATEGORY],[CODE_TYPE]) VALUES('94500-6','LAB-COVID','LC');
INSERT #COVID_Inclusion([CODE],[CODE_CATEGORY],[CODE_TYPE]) VALUES('94502-2','LAB-COVID','LC');
INSERT #COVID_Inclusion([CODE],[CODE_CATEGORY],[CODE_TYPE]) VALUES('94503-0','LAB-COVID','LC');
INSERT #COVID_Inclusion([CODE],[CODE_CATEGORY],[CODE_TYPE]) VALUES('94504-8','LAB-COVID','LC');
INSERT #COVID_Inclusion([CODE],[CODE_CATEGORY],[CODE_TYPE]) VALUES('94505-5','LAB-COVID','LC');
INSERT #COVID_Inclusion([CODE],[CODE_CATEGORY],[CODE_TYPE]) VALUES('94506-3','LAB-COVID','LC');
INSERT #COVID_Inclusion([CODE],[CODE_CATEGORY],[CODE_TYPE]) VALUES('94507-1','LAB-COVID','LC');
INSERT #COVID_Inclusion([CODE],[CODE_CATEGORY],[CODE_TYPE]) VALUES('94508-9','LAB-COVID','LC');
INSERT #COVID_Inclusion([CODE],[CODE_CATEGORY],[CODE_TYPE]) VALUES('94509-7','LAB-COVID','LC');
INSERT #COVID_Inclusion([CODE],[CODE_CATEGORY],[CODE_TYPE]) VALUES('94510-5','LAB-COVID','LC');
INSERT #COVID_Inclusion([CODE],[CODE_CATEGORY],[CODE_TYPE]) VALUES('94511-3','LAB-COVID','LC');
INSERT #COVID_Inclusion([CODE],[CODE_CATEGORY],[CODE_TYPE]) VALUES('94531-1','LAB-COVID','LC');
INSERT #COVID_Inclusion([CODE],[CODE_CATEGORY],[CODE_TYPE]) VALUES('94532-9','LAB-COVID','LC');
INSERT #COVID_Inclusion([CODE],[CODE_CATEGORY],[CODE_TYPE]) VALUES('94533-7','LAB-COVID','LC');
INSERT #COVID_Inclusion([CODE],[CODE_CATEGORY],[CODE_TYPE]) VALUES('94534-5','LAB-COVID','LC');
INSERT #COVID_Inclusion([CODE],[CODE_CATEGORY],[CODE_TYPE]) VALUES('94547-7','LAB-COVID','LC');
INSERT #COVID_Inclusion([CODE],[CODE_CATEGORY],[CODE_TYPE]) VALUES('94558-4','LAB-COVID','LC');
INSERT #COVID_Inclusion([CODE],[CODE_CATEGORY],[CODE_TYPE]) VALUES('94559-2','LAB-COVID','LC');
INSERT #COVID_Inclusion([CODE],[CODE_CATEGORY],[CODE_TYPE]) VALUES('94562-6','LAB-COVID','LC');
INSERT #COVID_Inclusion([CODE],[CODE_CATEGORY],[CODE_TYPE]) VALUES('94563-4','LAB-COVID','LC');
INSERT #COVID_Inclusion([CODE],[CODE_CATEGORY],[CODE_TYPE]) VALUES('94564-2','LAB-COVID','LC');
INSERT #COVID_Inclusion([CODE],[CODE_CATEGORY],[CODE_TYPE]) VALUES('94565-9','LAB-COVID','LC');
INSERT #COVID_Inclusion([CODE],[CODE_CATEGORY],[CODE_TYPE]) VALUES('94639-2','LAB-COVID','LC');
INSERT #COVID_Inclusion([CODE],[CODE_CATEGORY],[CODE_TYPE]) VALUES('94640-0','LAB-COVID','LC');
INSERT #COVID_Inclusion([CODE],[CODE_CATEGORY],[CODE_TYPE]) VALUES('94641-8','LAB-COVID','LC');
INSERT #COVID_Inclusion([CODE],[CODE_CATEGORY],[CODE_TYPE]) VALUES('94642-6','LAB-COVID','LC');
INSERT #COVID_Inclusion([CODE],[CODE_CATEGORY],[CODE_TYPE]) VALUES('94643-4','LAB-COVID','LC');
INSERT #COVID_Inclusion([CODE],[CODE_CATEGORY],[CODE_TYPE]) VALUES('94644-2','LAB-COVID','LC');
INSERT #COVID_Inclusion([CODE],[CODE_CATEGORY],[CODE_TYPE]) VALUES('94645-9','LAB-COVID','LC');
INSERT #COVID_Inclusion([CODE],[CODE_CATEGORY],[CODE_TYPE]) VALUES('94646-7','LAB-COVID','LC');
INSERT #COVID_Inclusion([CODE],[CODE_CATEGORY],[CODE_TYPE]) VALUES('94647-5','LAB-COVID','LC');
INSERT #COVID_Inclusion([CODE],[CODE_CATEGORY],[CODE_TYPE]) VALUES('94660-8','LAB-COVID','LC');
INSERT #COVID_Inclusion([CODE],[CODE_CATEGORY],[CODE_TYPE]) VALUES('94661-6','LAB-COVID','LC');
INSERT #COVID_Inclusion([CODE],[CODE_CATEGORY],[CODE_TYPE]) VALUES('94720-0','LAB-COVID','LC');
INSERT #COVID_Inclusion([CODE],[CODE_CATEGORY],[CODE_TYPE]) VALUES('94745-7','LAB-COVID','LC');
INSERT #COVID_Inclusion([CODE],[CODE_CATEGORY],[CODE_TYPE]) VALUES('94746-5','LAB-COVID','LC');
INSERT #COVID_Inclusion([CODE],[CODE_CATEGORY],[CODE_TYPE]) VALUES('94756-4','LAB-COVID','LC');
INSERT #COVID_Inclusion([CODE],[CODE_CATEGORY],[CODE_TYPE]) VALUES('94757-2','LAB-COVID','LC');
INSERT #COVID_Inclusion([CODE],[CODE_CATEGORY],[CODE_TYPE]) VALUES('94758-0','LAB-COVID','LC');
INSERT #COVID_Inclusion([CODE],[CODE_CATEGORY],[CODE_TYPE]) VALUES('94759-8','LAB-COVID','LC');
INSERT #COVID_Inclusion([CODE],[CODE_CATEGORY],[CODE_TYPE]) VALUES('94760-6','LAB-COVID','LC');
INSERT #COVID_Inclusion([CODE],[CODE_CATEGORY],[CODE_TYPE]) VALUES('94761-4','LAB-COVID','LC');
INSERT #COVID_Inclusion([CODE],[CODE_CATEGORY],[CODE_TYPE]) VALUES('94762-2','LAB-COVID','LC');
INSERT #COVID_Inclusion([CODE],[CODE_CATEGORY],[CODE_TYPE]) VALUES('94763-0','LAB-COVID','LC');
INSERT #COVID_Inclusion([CODE],[CODE_CATEGORY],[CODE_TYPE]) VALUES('94764-8','LAB-COVID','LC');
INSERT #COVID_Inclusion([CODE],[CODE_CATEGORY],[CODE_TYPE]) VALUES('94765-5','LAB-COVID','LC');
INSERT #COVID_Inclusion([CODE],[CODE_CATEGORY],[CODE_TYPE]) VALUES('94766-3','LAB-COVID','LC');
INSERT #COVID_Inclusion([CODE],[CODE_CATEGORY],[CODE_TYPE]) VALUES('94767-1','LAB-COVID','LC');
INSERT #COVID_Inclusion([CODE],[CODE_CATEGORY],[CODE_TYPE]) VALUES('94768-9','LAB-COVID','LC');
INSERT #COVID_Inclusion([CODE],[CODE_CATEGORY],[CODE_TYPE]) VALUES('94769-7','LAB-COVID','LC');
INSERT #COVID_Inclusion([CODE],[CODE_CATEGORY],[CODE_TYPE]) VALUES('94819-0','LAB-COVID','LC');
INSERT #COVID_Inclusion([CODE],[CODE_CATEGORY],[CODE_TYPE]) VALUES('94822-4','LAB-COVID','LC');
INSERT #COVID_Inclusion([CODE],[CODE_CATEGORY],[CODE_TYPE]) VALUES('94845-5','LAB-COVID','LC');
INSERT #COVID_Inclusion([CODE],[CODE_CATEGORY],[CODE_TYPE]) VALUES('95125-1','LAB-COVID','LC');
INSERT #COVID_Inclusion([CODE],[CODE_CATEGORY],[CODE_TYPE]) VALUES('95209-3','LAB-COVID','LC');
-----------------------------------------------------------------------------------------
--Insert the LOINC codes for Vitamin D labs that define the population
-----------------------------------------------------------------------------------------
INSERT #COVID_Inclusion([CODE],[CODE_CATEGORY],[CODE_TYPE]) VALUES('49054-0','LAB-VITD','LC');
INSERT #COVID_Inclusion([CODE],[CODE_CATEGORY],[CODE_TYPE]) VALUES('62292-8','LAB-VITD','LC');
INSERT #COVID_Inclusion([CODE],[CODE_CATEGORY],[CODE_TYPE]) VALUES('2236-8','LAB-VITD','LC');
INSERT #COVID_Inclusion([CODE],[CODE_CATEGORY],[CODE_TYPE]) VALUES('1989-3','LAB-VITD','LC');

-----------------------------------------------------------------------------------------
--Create a temporary table that holds the earliest COVID lab test for all patients.
--		In defining the earliest date, lab order date is used if present, else, result date is used
-----------------------------------------------------------------------------------------
IF OBJECT_ID('tempdb.dbo.#DENOM_LABS', 'U') IS NOT NULL DROP TABLE #DENOM_LABS; 

CREATE TABLE #DENOM_LABS(
	[PATID] [varchar](128) NOT NULL,
	[FIRST_COVID_TEST_DATE] date NULL,
	[RECENT_VITD_LAB_TEST_DATE] date NULL
) 

INSERT #DENOM_LABS(PATID, FIRST_COVID_TEST_DATE)
select l.PATID, MIN(COALESCE(l.LAB_ORDER_DATE, l.RESULT_DATE)) as FIRST_COVID_TEST_DATE
from LAB_RESULT_CM l
	join #COVID_Inclusion ci on l.LAB_LOINC = ci.CODE and ci.CODE_CATEGORY='LAB-COVID' 
group by l.PATID;

UPDATE #DENOM_LABS
SET RECENT_VITD_LAB_TEST_DATE=a.RECENT_VITD_LAB_TEST_DATE
FROM #DENOM_LABS
	join 
	(
	select l.PATID, MAX(COALESCE(l.LAB_ORDER_DATE, l.RESULT_DATE)) as [RECENT_VITD_LAB_TEST_DATE]
	from LAB_RESULT_CM l
		join #COVID_Inclusion ci on l.LAB_LOINC = ci.CODE and ci.CODE_CATEGORY='LAB-VITD' 
		join #DENOM_LABS dl on dl.PATID=l.PATID 
	where DATEDIFF(DAY,COALESCE(l.LAB_ORDER_DATE, l.RESULT_DATE), dl.FIRST_COVID_TEST_DATE) between 14 and 365
	group by l.PATID
	) a on a.PATID=#DENOM_LABS.PATID


IF OBJECT_ID('tempdb.dbo.#DENOM', 'U') IS NOT NULL DROP TABLE #DENOM; 

CREATE TABLE #DENOM(
	[PATID] [varchar](128) NOT NULL,
	[FIRST_COVID_TEST_DATE] date NULL,
	[RECENT_VITD_LAB_TEST_DATE] date NULL
) 

INSERT #DENOM(PATID, FIRST_COVID_TEST_DATE, RECENT_VITD_LAB_TEST_DATE)
select PATID, FIRST_COVID_TEST_DATE, RECENT_VITD_LAB_TEST_DATE
from #DENOM_LABS 
where RECENT_VITD_LAB_TEST_DATE is not null
	and FIRST_COVID_TEST_DATE is not null


-----------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------
--RETURN DATA
-----------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------

-----------------------------------------------------------------------------------------
--RETURN DATAVANT HASH TOKENS
-----------------------------------------------------------------------------------------
select CAP_HASH_TOKEN.[CAP_ID], CAP_HASH_TOKEN.[TOKEN_01], CAP_HASH_TOKEN.[TOKEN_02], CAP_HASH_TOKEN.[TOKEN_03]
	, CAP_HASH_TOKEN.[TOKEN_04], CAP_HASH_TOKEN.[TOKEN_05], CAP_HASH_TOKEN.[TOKEN_16]
from [CAPriCORN_CDM_6.0_LDS].dbo.CAP_HASH_TOKEN		--!! Enter database name where CAPriCORN CAP_HASH_TOKEN table is found
	join #DENOM on #DENOM.PATID=CAP_HASH_TOKEN.CAP_ID

-----------------------------------------------------------------------------------------
--RETURN DEMOGRAPHIC DATA
-----------------------------------------------------------------------------------------
select DEMOGRAPHIC.PATID, DEMOGRAPHIC.SEX, DEMOGRAPHIC.RACE, DEMOGRAPHIC.HISPANIC
	, FLOOR(DATEDIFF(day, DEMOGRAPHIC.BIRTH_DATE, #DENOM.FIRST_COVID_TEST_DATE)/365.25) as AGE_AT_FIRST_COVID_TEST
from DEMOGRAPHIC
	join #DENOM on #DENOM.PATID=DEMOGRAPHIC.PATID

-----------------------------------------------------------------------------------------
--RETURN ALL ENCOUNTERS IN THE 730 DAYS PRIOR TO FIRST COVID TEST
-----------------------------------------------------------------------------------------
select ENCOUNTER.[PATID], ENCOUNTER.[ENCOUNTERID], ENCOUNTER.[ADMIT_DATE], ENCOUNTER.[DISCHARGE_DATE], ENCOUNTER.[ENC_TYPE]
	, ENCOUNTER.PAYER_TYPE_PRIMARY, ENCOUNTER.PAYER_TYPE_SECONDARY
from ENCOUNTER
	join #DENOM on #DENOM.PATID=ENCOUNTER.PATID
WHERE DATEDIFF(day, ENCOUNTER.ADMIT_DATE, #DENOM.FIRST_COVID_TEST_DATE) < 730

-----------------------------------------------------------------------------------------
--RETURN DIAGNOSES DATA IN THE 730 DAYS PRIOR TO FIRST COVID TEST
-----------------------------------------------------------------------------------------
select DIAGNOSIS.PATID, DIAGNOSIS.ENCOUNTERID, DIAGNOSIS.DX_DATE, DIAGNOSIS.ADMIT_DATE, DIAGNOSIS.DX
from DIAGNOSIS
	join #DENOM on #DENOM.PATID=DIAGNOSIS.PATID 
WHERE DIAGNOSIS.DX_TYPE='10' 
	and DATEDIFF(day, COALESCE(DIAGNOSIS.DX_DATE, DIAGNOSIS.ADMIT_DATE), #DENOM.FIRST_COVID_TEST_DATE) < 730

-----------------------------------------------------------------------------------------
--RETURN VITAMIN D LAB DATA IN THE 365 DAYS PRIOR TO FIRST COVID TEST
-----------------------------------------------------------------------------------------
select LAB_RESULT_CM.[PATID], LAB_RESULT_CM.ENCOUNTERID, LAB_RESULT_CM.[SPECIMEN_SOURCE], LAB_RESULT_CM.[LAB_LOINC], LAB_RESULT_CM.[LAB_RESULT_SOURCE]
	, LAB_RESULT_CM.[LAB_ORDER_DATE], LAB_RESULT_CM.[RESULT_DATE], LAB_RESULT_CM.[RESULT_QUAL], LAB_RESULT_CM.[RESULT_SNOMED]
	, LAB_RESULT_CM.[RESULT_NUM], LAB_RESULT_CM.[RESULT_MODIFIER], LAB_RESULT_CM.[RESULT_UNIT], [RAW_LAB_NAME]
from LAB_RESULT_CM
	join #DENOM on #DENOM.PATID=LAB_RESULT_CM.PATID
	join #COVID_Inclusion on LAB_RESULT_CM.LAB_LOINC = #COVID_Inclusion.CODE and #COVID_Inclusion.CODE_CATEGORY='LAB-VITD' 
WHERE DATEDIFF(day, COALESCE(LAB_RESULT_CM.LAB_ORDER_DATE, LAB_RESULT_CM.RESULT_DATE), #DENOM.FIRST_COVID_TEST_DATE) < 365

-----------------------------------------------------------------------------------------
--RETURN ALL COVID LAB DATA
-----------------------------------------------------------------------------------------
select LAB_RESULT_CM.[PATID], LAB_RESULT_CM.ENCOUNTERID, LAB_RESULT_CM.[SPECIMEN_SOURCE], LAB_RESULT_CM.[LAB_LOINC], LAB_RESULT_CM.[LAB_RESULT_SOURCE]
	, LAB_RESULT_CM.[LAB_ORDER_DATE], LAB_RESULT_CM.[RESULT_DATE], LAB_RESULT_CM.[RESULT_QUAL], LAB_RESULT_CM.[RESULT_SNOMED]
	, LAB_RESULT_CM.[RESULT_NUM], LAB_RESULT_CM.[RESULT_MODIFIER], LAB_RESULT_CM.[RESULT_UNIT], [RAW_LAB_NAME]
from LAB_RESULT_CM
	join #DENOM on #DENOM.PATID=LAB_RESULT_CM.PATID
	join #COVID_Inclusion on LAB_RESULT_CM.LAB_LOINC = #COVID_Inclusion.CODE and #COVID_Inclusion.CODE_CATEGORY='LAB-COVID' 

-----------------------------------------------------------------------------------------
--RETURN PRESCRIBED MEDS DATA IN THE 730 DAYS PRIOR TO FIRST COVID TEST
-----------------------------------------------------------------------------------------
select PRESCRIBING.[PATID], PRESCRIBING.ENCOUNTERID, PRESCRIBING.[RX_ORDER_DATE], PRESCRIBING.[RX_START_DATE], PRESCRIBING.[RX_END_DATE], PRESCRIBING.[RX_DOSE_ORDERED], PRESCRIBING.[RX_DOSE_ORDERED_UNIT]
	, PRESCRIBING.[RX_QUANTITY], PRESCRIBING.[RX_DOSE_FORM], PRESCRIBING.[RX_REFILLS], PRESCRIBING.[RX_DAYS_SUPPLY], PRESCRIBING.[RX_FREQUENCY], PRESCRIBING.[RX_ROUTE]
	, PRESCRIBING.[RX_BASIS], PRESCRIBING.[RXNORM_CUI], PRESCRIBING.[RAW_RX_MED_NAME]
from PRESCRIBING
	join #DENOM on #DENOM.PATID=PRESCRIBING.PATID
WHERE DATEDIFF(day, COALESCE(PRESCRIBING.[RX_START_DATE],PRESCRIBING.RX_ORDER_DATE), #DENOM.FIRST_COVID_TEST_DATE) < 730

-----------------------------------------------------------------------------------------
--RETURN ADMINISTERED MEDS DATA IN THE 730 DAYS PRIOR TO FIRST COVID TEST
-----------------------------------------------------------------------------------------
select MED_ADMIN.[PATID], MED_ADMIN.ENCOUNTERID, MED_ADMIN.[MEDADMIN_START_DATE], MED_ADMIN.[MEDADMIN_STOP_DATE], MED_ADMIN.[MEDADMIN_TYPE], MED_ADMIN.[MEDADMIN_CODE], MED_ADMIN.[MEDADMIN_DOSE_ADMIN]
	, MED_ADMIN.[MEDADMIN_DOSE_ADMIN_UNIT], MED_ADMIN.[MEDADMIN_ROUTE], MED_ADMIN.[MEDADMIN_SOURCE], MED_ADMIN.[RAW_MEDADMIN_MED_NAME]
from MED_ADMIN
	join #DENOM on #DENOM.PATID=MED_ADMIN.PATID
WHERE DATEDIFF(day, MED_ADMIN.[MEDADMIN_START_DATE], #DENOM.FIRST_COVID_TEST_DATE) < 730

-----------------------------------------------------------------------------------------
--RETURN BMI, HEIGHT, WEIGHT DATA IN THE 365 DAYS PRIOR TO FIRST COVID TEST
-----------------------------------------------------------------------------------------
select VITAL.[PATID], VITAL.ENCOUNTERID, VITAL.[MEASURE_DATE], VITAL.[HT], VITAL.[WT], VITAL.[ORIGINAL_BMI]
from VITAL
	join #DENOM on #DENOM.PATID=VITAL.PATID
WHERE DATEDIFF(day, VITAL.MEASURE_DATE, #DENOM.FIRST_COVID_TEST_DATE) < 365
	and (VITAL.HT is not null or VITAL.WT is not null or VITAL.ORIGINAL_BMI is not null)




